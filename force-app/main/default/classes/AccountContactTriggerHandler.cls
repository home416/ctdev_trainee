public with sharing class AccountContactTriggerHandler {
    /**
     * If reference object gets created for the first time and
     * there are no other working places for this contact
     * isPrimary=true.
     */
    public static void onInsertCheckIsFirst(List<AccountContact__c> newList) {
        List<AccountContact__c> total = AccountContactTriggerHelper.getRelatedContacts(newList);

        for (AccountContact__c variable : newList) {
            //first on contact
            if (!total.contains(variable)) {
                variable.isPrimary__c = true;
            }
        }
    }

    public static void onUpdateChangeOther(List<AccountContact__c> newList) {
        List<AccountContact__c> total = AccountContactTriggerHelper.getRelatedContacts(newList);
        for (AccountContact__c variable : newList) {
            List<AccountContact__c> relatedRecords = new List<AccountContact__c>();
            for (AccountContact__c target : total) {
                if (target.Contact__c == variable.Contact__c) {
                    relatedRecords.add(target);
                }
            }
            if (relatedRecords.size() == 0) {
                continue;
            }

            if (variable.isPrimary__c == true) {
                for (AccountContact__c target : relatedRecords) {
                    target.isPrimary__c = false;
                }
            } else {
                AccountContact__c max = relatedRecords.get(0);
                for (AccountContact__c target : relatedRecords) {
                    if (target.Name.compareTo(max.Name) > 0) {
                        max = target;
                    }
                }
                for (AccountContact__c target : relatedRecords) {
                    target.isPrimary__c = false;
                }
                max.isPrimary__c = true;
            }
        }
    }
}

@isTest
public with sharing class AccountContactTriggerTest {
    @IsTest
    static void triggerTest() {
        Account acc = new Account();
        acc.Name = 'testAcc';
        insert acc;

        Contact con = new Contact();
        con.LastName = 'testCon';
        insert con;

        AccountContact__c ac1 = new AccountContact__c();
        ac1.Account__c = acc.Id;
        ac1.Contact__c = con.Id;
        ac1.isPrimary__c = false;
        insert ac1;

        Test.startTest();

        Test.stopTest();
    }
}

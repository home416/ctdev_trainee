public with sharing class AccountContactTriggerHelper {
    public static List<AccountContact__c> getRelatedContacts(List<AccountContact__c> newList) {
        Set<Id> targetContacts = new Set<Id>();
        for (AccountContact__c variable : newList) {
            targetContacts.add(variable.Contact__c);
        }

        List<AccountContact__c> total = [
            SELECT Id, isPrimary__c, Name
            FROM AccountContact__c
            WHERE Contact__c IN :targetContacts
            WITH SECURITY_ENFORCED
            ORDER BY Name
        ];
        return total;
    }
}

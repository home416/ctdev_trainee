trigger AccountContactTrigger on AccountContact__c(before insert, before update, before delete) {
    System.debug('hello');

    List<AccountContact__c> total = [SELECT Id FROM AccountContact__c];
    System.debug(total.size());

    if (Trigger.isBefore) {
        if (Trigger.isInsert) {
            AccountContactTriggerHandler.onInsertCheckIsFirst(Trigger.new);
        }

        if (Trigger.isUpdate) {
            AccountContactTriggerHandler.onUpdateChangeOther(Trigger.new);
        }
        if (Trigger.isDelete) {
        }
    }
}
